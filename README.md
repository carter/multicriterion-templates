#MultiCriterion Template

This is the first of a collection of alternative html templates for 
Haskell Criterion benchmark reports.

when using the report template, each top level *bgroup* will have 
a seperate comparison bar chart.

an example invocation would be 

```   ./criterion-benchmarks  -o "benchmark.html" --template=templates/report.tpl```

BSD3 license